package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")

// Marks this java object as a representation of an entity record from the database table
public class Post {
    //  Properties
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String title;
    @Column
    private String content;

    public Post() {
        //default constructor
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }
    //    Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}